package org.lucario;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice1 = 0;
        int choice2 = 0;

        do {
            System.out.println("-------------------------------");
            System.out.println("Aplikasi Pengolah Nilai Siswa");
            System.out.println("-------------------------------");
            System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori berikut: C://temp/direktori");
            System.out.println();
            System.out.println("Pilih menu: ");
            System.out.println("1. Generate txt untuk menampilkan modus");
            System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median");
            System.out.println("3. Generate kedua file");
            System.out.println("0. Exit");
            choice1 = scanner.nextInt();

            int result = 0;

            if (choice1 == 0) {
                System.out.println("Menutup Aplikasi");
            } else if (choice1 == 1) {
                System.out.println("Anda memilih pilihan ke 1");
                result = Modus.calculateModusAndSaveToFile();
            } else if (choice1 == 2) {
                System.out.println("Anda memilih pilihan ke 2");
                result = Modus.calculateMeanMedianModusAndSaveToFile();
            } else if (choice1 == 3) {
                System.out.println("Anda memilih pilihan ke 3");
                result = Modus.calculateModusAndSaveToFile();
                result = Modus.calculateMeanMedianModusAndSaveToFile();
            } else {
                System.out.println("Pilihan Tidak Ditemukan !!!");
            }

            // Tambahkan bagian untuk memunculkan pilihan setelah hasil perhitungan
            if (result == 1) {
                System.out.println("Pilihan setelah perhitungan:");
                System.out.println("1. Mulai kembali dari awal");
                System.out.println("0. Exit");
                choice2 = scanner.nextInt();

                if (choice2 == 0) {
                    System.out.println("Menutup Aplikasi");
                } else if (choice2 != 1) {
                    System.out.println("Pilihan Tidak Ditemukan !!!");
                }
            }
        } while (choice1 != 0 && choice2 == 1);

        scanner.close();
    }
}