package org.lucario;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class Modus {
    public static int calculateModusAndSaveToFile() {
        String inputFilePath = "C://temp/direktori/data_sekolah.csv";
        String outputFilePath = "C://temp/direktori/data_sekolah_modus.txt";
        int res = 0;

        try (FileInputStream fis = new FileInputStream(inputFilePath);
             InputStreamReader isr = new InputStreamReader(fis);
             BufferedReader reader = new BufferedReader(isr);
             FileWriter fileWriter = new FileWriter(outputFilePath);
             BufferedWriter writer = new BufferedWriter(fileWriter)) {

            Map<String, Integer> frequencyMap = new HashMap<>();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(";");
                List<Integer> nilaiList = new ArrayList<>();

                for (int i = 1; i < parts.length; i++) {
                    nilaiList.add(Integer.parseInt(parts[i]));
                }

                Map<String, Integer> classFrequencyMap = calculateFrequency(nilaiList);

                // Menambahkan frekuensi dari kelas ke frekuensi total
                addToTotalFrequency(frequencyMap, classFrequencyMap);
            }

            // Menyimpan hasil modus ke file
            writeResultsToFile(writer, frequencyMap);

            System.out.println("Pengolahan nilai dan penyimpanan modus ke file telah selesai. Hasil disimpan di " + outputFilePath);

        } catch (IOException e) {
//            e.printStackTrace();
            String error = e.getMessage();
            if(error.contains("(The system cannot find the file specified)")){
                System.out.println("File tidak ditemukan");
            }else {
                System.out.println(e.getMessage());
            }
            res = 1;
        } finally {
            return res;
        }
    }

    public static int calculateMeanMedianModusAndSaveToFile() {
        String inputFilePath = "C://temp/direktori/data_sekolah.csv";
        String outputFilePath = "C://temp/direktori/data_sekolah_modus_median.txt";
        int res = 0;

        try (FileInputStream fis = new FileInputStream(inputFilePath);
             InputStreamReader isr = new InputStreamReader(fis);
             BufferedReader reader = new BufferedReader(isr);
             FileWriter fileWriter = new FileWriter(outputFilePath);
             BufferedWriter writer = new BufferedWriter(fileWriter)) {

            List<Integer> allValues = new ArrayList<>();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(";");
                for (int i = 1; i < parts.length; i++) {
                    allValues.add(Integer.parseInt(parts[i]));
                }
            }

            // Menghitung mean, median, dan modus
            double mean = calculateMean(allValues);
            double median = calculateMedian(allValues);
            Map<Integer, Integer> modusMap = calculateModus(allValues);

            // Menyimpan hasil mean, median, dan modus ke file
            writeMeanMedianModusResultsToFile(writer, mean, median, modusMap);

            System.out.println("Pengolahan nilai dan penyimpanan mean, median, dan modus ke file telah selesai. Hasil disimpan di " + outputFilePath);

        } catch (IOException e) {
//            e.printStackTrace();
            String error = e.getMessage();
            if(error.contains("(The system cannot find the file specified)")){
                System.out.println("File tidak ditemukan");
            }else {
                System.out.println(e.getMessage());
            }
            res = 1;
        } finally {
            return res;
        }
    }

    private static Map<String, Integer> calculateFrequency(List<Integer> nilaiList) {
        Map<String, Integer> frequencyMap = new HashMap<>();

        for (int nilai : nilaiList) {
            String key = (nilai < 6) ? "kurang dari 6" : Integer.toString(nilai);
            frequencyMap.put(key, frequencyMap.getOrDefault(key, 0) + 1);
        }

        return frequencyMap;
    }

    private static void addToTotalFrequency(Map<String, Integer> totalFrequencyMap, Map<String, Integer> classFrequencyMap) {
        // Menambahkan frekuensi dari kelas ke frekuensi total
        for (Map.Entry<String, Integer> entry : classFrequencyMap.entrySet()) {
            totalFrequencyMap.put(entry.getKey(), totalFrequencyMap.getOrDefault(entry.getKey(), 0) + entry.getValue());
        }
    }

    private static double calculateMean(List<Integer> values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return (double) sum / values.size();
    }

    private static double calculateMedian(List<Integer> values) {
        Collections.sort(values);
        int size = values.size();
        if (size % 2 == 0) {
            return (values.get(size / 2 - 1) + values.get(size / 2)) / 2.0;
        } else {
            return values.get(size / 2);
        }
    }

    private static Map<Integer, Integer> calculateModus(List<Integer> values) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();

        for (int value : values) {
            frequencyMap.put(value, frequencyMap.getOrDefault(value, 0) + 1);
        }

        return frequencyMap;
    }

    private static void writeResultsToFile(BufferedWriter writer, Map<?, Integer> frequencyMap) throws IOException {
        // Menuliskan hasil modus ke file
        for (Map.Entry<?, Integer> entry : frequencyMap.entrySet()) {
            writer.write(entry.getKey() + "\t\t\t|" + entry.getValue() + "\n");
        }
        writer.write("\n");
    }

    private static void writeMeanMedianModusResultsToFile(BufferedWriter writer, double mean, double median, Map<Integer, Integer> modusMap) throws IOException {
        // Menuliskan hasil mean, median, dan modus ke file
        DecimalFormat df = new DecimalFormat("#.##");

        writer.write("Berikut Hasil Sebaran Data Nilai:\n");
        writer.write("Mean: " + df.format(mean) + "\n");
        writer.write("Median: " + df.format(median) + "\n");
        int tempMod = 0;
        int tempVal = 0;
        for (Map.Entry<Integer, Integer> entry : modusMap.entrySet()) {
            if(entry.getValue() > tempVal){
                tempMod = entry.getKey();
                tempVal = entry.getValue();
            }

        }
        writer.write("Modus: "+ tempMod +"\n");
        writer.write("\n");
    }
}
